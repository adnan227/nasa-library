Sainsbury's Nasa Technical Test in React
=======

## Dependencies

This app requires Masonry to be installed by running

`npm install`

## Running

To run the app, you need to run

`npm start`

Load in the browser and search for a term

## Limitations

* Searches both Audio and Video (no separation at the moment)
* There are some elements when loaded, it doesn't have any information. Better error handling is required for this by either logging (and not showing anything for that element) or creating an alert for it
* The layout doesn't fit together like it does on the Nasa app. Use Masonry options to have the correct layout

## More Time?

If I had more time, I would implement the following things:

* Allow the choice of choosing either Audio or Image or Both (checkbox)
* Create a modal pop-up that contains the title, description and image
* Changed it so that it didn't update on submit, but updated as you were typing.
* Lazy loading - so that it loaded as you scroll
* Animated loading + animated modal popup
* Error handling - so that it actually logs what went wrong
* Having more information based on the search queries
