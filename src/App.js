import React, { Component } from 'react';
import Masonry from 'react-masonry-component';
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            images: [],
            input: '',
            error: null,
            nasa_id: '',
            href: '',
            masonryOptions: {
                transitionDuration: 10,
                columnWidth: 20,
                gutter: 5
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ input: event.target.value });
    }

    handleSubmit(event) {
        // stop the submit from actually submitting
        event.preventDefault();

        // get the data from the nasa website and append the search input - currently gets all the images and audio
        fetch('https://images-api.nasa.gov/search?q=' + this.state.input + '&media_type=image,audio')
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    images: result.collection.items
                });
            },
            (error) => {
                this.setState({
                    error
                });
            }
        );
    }

    renderImage(image) {
        // make sure the information actually exists
        if (image.links !== undefined && image.data !== undefined) {
            return (
                <div key={image.data["0"].nasa_id} >
                    <div className="grid-sizer"/>
                    <div key={image.data["0"].nasa_id} className="grid-item">
                        <img src={image.links["0"].href} alt=""/>
                    </div>
                </div>
            );
        } else {
            // returns an empty box with "NOT FOUND" inside it
            return (
                <div key={image.href} className="image-element-not-found">
                    NOT FOUND
                </div>
            );
        }
    }

    render() {
        const { error, images, masonryOptions } = this.state;

        if (error) {
            return <div>Error: { error.message }</div>
        } else {
            if (images) {
                // load the images into a div
                const imagesList = images.map((image) =>
                    this.renderImage(image)
                );

                return (
                    <div className="App">
                        <header className="App-header">
                            <h1 className="App-title">NASA Search</h1>
                            <form onSubmit={this.handleSubmit}>
                                <input type="text" value={this.state.input} onChange={ this.handleChange } />
                                <input type="submit" value="Search"/>
                            </form>
                        </header>
                        <div className="grid">
                            <Masonry
                                className={'my-gallery-class'}
                                elementType={'div'}
                                options={masonryOptions}
                                disableImagesLoaded={false}
                                updateOnEachImageLoad={false}
                            >
                                {imagesList}
                            </Masonry>
                        </div>
                    </div>
                );
            }
        }
    }
}

export default App;
